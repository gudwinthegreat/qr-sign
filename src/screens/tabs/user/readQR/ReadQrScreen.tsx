import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { Camera, CameraPermissionRequestResult, useCameraDevices, useFrameProcessor } from 'react-native-vision-camera';
import { scanQRCodes, QrCode } from 'vision-camera-qrcode-scanner';
import { Linking, ActivityIndicator, StyleSheet, TouchableOpacity, View } from 'react-native';
import { runOnJS } from 'react-native-reanimated';
import { Colors } from "../../../../styles/Colors";


export const ReadQrScreen = observer(() => {

    const [cameraPermission, setCameraPermission] = useState<CameraPermissionRequestResult>("denied");
    const [shouldRefresh, setRefresh] = useState(false)
    const [qrCode, setQrCode] = React.useState<QrCode>();

    const devices = useCameraDevices()
    const device = devices.back

    const frameProcessor = useFrameProcessor((frame) => {
        'worklet';
        const qrcode = scanQRCodes(frame);

        if (!!qrcode[0]) {
            runOnJS(setQrCode)(qrcode[0]);
        }
    }, []);

    useEffect(() => {
        if (!!qrCode) {
            console.log(qrCode)
        }
    }, [qrCode])

    useEffect(() => {
        Camera.requestCameraPermission()
            .then(cameraPermission => {
                setCameraPermission(cameraPermission)
                // https://reactnative.dev/docs/linking#opensettings
                if (cameraPermission !== "authorized") {
                    Linking.openSettings().finally(() => {
                        setRefresh((refresh) => !refresh)
                    });
                }
            })
            .catch(e => console.log(e))
    }, [shouldRefresh])
    return (
        <View style={styles.container}>
            {!!device && cameraPermission === "authorized" &&
                <Camera
                    style={StyleSheet.absoluteFill}
                    device={device}
                    isActive={true}
                    frameProcessor={frameProcessor}
                    frameProcessorFps={5}
                />
            }
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 48,
        backgroundColor: Colors.black,
        flexDirection: 'row',
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: 'space-between',
    }
})