import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { useRootStore } from '../../../../base/hooks/useRootStore';
import { View } from "react-native";
import { IInput } from "../../../../base/components/IInput";
import { Button } from "../../../../base/components/Button";
import { Role } from "../../../../modules/main/models/MainModels";
import Navigation from "../../../../base/Navigation";
import { screens } from "../../../../navigation/consts/screens";

export const CreateQrScreen = observer(() => {
    const { mainStore, userStore } = useRootStore();

    useEffect(() => {
        console.log(mainStore.role)
    }, [])

    //TODO
    const handleChange = (value:string) => {
        console.log(value)
        userStore.setCheckString(value)        
    }

    // TODO
    const handleCreate = () => {
        Navigation.navigate(screens.USER_SHOW_QR)
    }

    //TODO
    const saveLocal = () => {}

    return (
        mainStore.role===Role.USER?
        <View>
            <IInput
            inputKey=""
            headerPlaceholder="введите проверочный текст"
            handleChange={handleChange}
            defaultValue={userStore.checkString}
            />
            <Button
            title="Создать QR"
            onPress={handleCreate}
            disabled={userStore.isCheckStringEmpty}
            />
            <Button
            title="Запомнить проверочный текст"
            onPress={saveLocal}
            />
        </View>:
        null
    )
})