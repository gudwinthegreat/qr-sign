import { observer } from "mobx-react-lite"
import React from "react"

import QRCode from 'react-native-qrcode-svg';
import { View } from "react-native";
import { Button } from "../../../../base/components/Button";
import { useRootStore } from "../../../../base/hooks/useRootStore";


export const ShowQrScreen = observer(() => {

    const { userStore } = useRootStore();

    const handleSaveLocal = () => {
        //TODO see example: https://openbase.com/js/react-native-qrcode-svg
    }
    return (
        <View>
            <QRCode
                value={userStore.checkString}
            />
            <Button
            title="Сохранить на устройстве"
            onPress={handleSaveLocal}
            />
        </View>)
})