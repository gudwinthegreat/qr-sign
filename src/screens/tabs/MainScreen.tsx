import React from 'react';
import { observer } from 'mobx-react-lite';
import { StyleSheet, View, Text } from 'react-native';
import { Button } from '../../base/components/Button';
import Navigation from '../../base/Navigation';
import { screens } from '../../navigation/consts/screens';

export const MainScreen = observer(() => {

    const createQR = () => {
        Navigation.navigate(screens.USER_CREATE_QR);
    }

    const readQR = () => {
        Navigation.navigate(screens.USER_READ_QR);
    }
    return (
        <View>
            <Button
                title='Создать QR'
                onPress={() => {
                    createQR()
                }}
            />
            <Button
                title='Прочитать QR'
                onPress={() => {
                    readQR()
                }}
            />
        </View>
    )
})