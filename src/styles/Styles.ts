import { StyleSheet } from 'react-native';

import { Colors } from './Colors';

export const GlobalStyles = StyleSheet.create({
  headerContainer: {
    backgroundColor: Colors.white,
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  flexRowBetween: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  avatarContainer: {
    width: 32,
    height: 32,
    borderRadius: 32 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.orangePale,
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  tabsContainer: {
    marginTop: 16,
    borderRadius: 8,
    backgroundColor: Colors.fillsNeutral300,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabItem: {
    borderRadius: 8,
    marginVertical: 4,
    paddingVertical: 8,
    marginHorizontal: '1%',
    width: '48%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabItemActive: {
    backgroundColor: Colors.white,
  },
  containerBadge: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: Colors.red,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 4,
  },
  alignCenter: {
    textAlign: 'center',
  },
});
