import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { AgEnum, Text } from '../../../base/components/Text';

import { screens } from '../../consts/screens';

import { Colors } from '../../../styles/Colors';

import { BottomTabBadge } from './BottomTabBadge';

export const BottomTabBar = (props: BottomTabBarProps) => {
  const { state, descriptors, navigation } = props;

  const insets = useSafeAreaInsets();

  const renderIcon = (route: any, isFocused: boolean) => {
    switch (route.name) {
      case screens.tab.TAB_USER:
        return <></>
      case screens.tab.TAB_ADMINISTRATOR:
        return <></>
      case screens.tab.TAB_COLLECTION:
        return <></>
      default:
        return <></>;
    }
  };

  return (
    <View style={[styles.container, { paddingBottom: insets.bottom }]}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const badge = options.tabBarBadge || null;
        const label =
          options.tabBarLabel !== undefined ? options.tabBarLabel : options.title !== '' ? options.title : route.name;
        const isFocused = state.index === index;

        const handlePress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={handlePress}
            style={styles.button}
            key={index}
          >
            <View>
              {badge && <BottomTabBadge badge={badge} />}
              {renderIcon(route, isFocused)}
            </View>
            <Text Ag={AgEnum.RN300} style={[{ color: isFocused ? Colors.brand400 : Colors.black }, styles.textLabel]}>
              {label as string}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    height: 74,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLabel: {
    paddingTop: 4,
  },
});
