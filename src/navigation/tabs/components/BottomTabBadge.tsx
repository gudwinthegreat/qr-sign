import React from 'react';
import { StyleSheet, View } from 'react-native';

import { AgEnum, Text } from '../../../base/components/Text';

import { Colors } from '../../../styles/Colors';

interface IBottomTabBadge {
  badge: string | number | null;
}

export const BottomTabBadge = ({ badge }: IBottomTabBadge) => {
  return (
    <View style={styles.containerBadge}>
      <View style={styles.badge}>
        <Text Ag={AgEnum.BottomBadge} color={Colors.white}>
          {badge?.toString()}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerBadge: {
    top: -10,
    left: 12,
    width: 25,
    height: 25,
    zIndex: 99999,
    borderRadius: 15,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
  },
  badge: {
    width: 20,
    height: 20,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.red,
  },
});
