import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { BottomTabBar } from './components/BottomTabBar';

import { screens } from '../consts/screens';
import Navigation from '../../base/Navigation';
import UserStack from '../stacks/UserStack';
import AdministratorStack from '../stacks/AdministratorStack';
import CollectionStack from '../stacks/CollectionStack';

const Tab = createBottomTabNavigator();

const NullComponent = () => {
  return null
}

export const Tabs = () => {

  return (
    <Tab.Navigator
      initialRouteName={Navigation.initialRoute}
      screenOptions={{
        tabBarLabelStyle: {
          paddingBottom: 4
        }
      }}
      tabBar={props => <BottomTabBar {...props} />}
    >
      <Tab.Screen
        name={screens.tab.TAB_USER}
        component={UserStack}
        options={{
          tabBarLabel: 'Пользователь',
          headerShown: false
        }}
      />

      <Tab.Screen
        name={screens.tab.TAB_ADMINISTRATOR}
        component={AdministratorStack}
        options={{
          tabBarLabel: 'Администратор',
          headerShown: false
        }}
      />

      <Tab.Screen
        name={screens.tab.TAB_COLLECTION}
        component={CollectionStack}
        options={{
          tabBarLabel: 'Коллекция',
          headerShown: false
        }}
      />
    </Tab.Navigator>
  );
};
