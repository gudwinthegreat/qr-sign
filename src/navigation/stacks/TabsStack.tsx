import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Tabs } from '../tabs/Tabs';

import { screens } from '../consts/screens';

const Stack = createStackNavigator();

export const TabsStack = (
  <>
    <Stack.Screen name={screens.MAIN_APP} component={Tabs} />
  </>
);
