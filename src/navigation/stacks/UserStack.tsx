import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { screens } from '../consts/screens';
import { MainScreen } from '../../screens/tabs/MainScreen';
import { Header } from '../../base/components/Header';
import { CloseButton } from '../../base/components/CloseButton';

import Navigation from '../../base/Navigation'
import {ShowQrScreen} from '../../screens/tabs/user/showQR/ShowQrScreen';
import {ReadQrScreen} from '../../screens/tabs/user/readQR/ReadQrScreen'
import {CreateQrScreen} from '../../screens/tabs/user/createQR/CreateQrScreen'

const Stack = createStackNavigator();

export default () => {

  const handleBackNavigation = () => {
    Navigation.resetToHome();
  };

  const handleBackNavigationToPreviousScreen = () => {
    Navigation.pop();
  }

  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screens.USER_STACK}
        component={MainScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header: () => <Header title={'Пользователь'} />
        }} />
      <Stack.Screen
        name={screens.USER_CREATE_QR}
        component={CreateQrScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header: () => <Header title={'Create QR'} rightComponent={<CloseButton onPress={handleBackNavigation} />} />
        }} />
        <Stack.Screen
        name={screens.USER_SHOW_QR}
        component={ShowQrScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header: () => <Header title={'Show QR'} rightComponent={<CloseButton onPress={handleBackNavigation} />} />
        }} />
        <Stack.Screen
        name={screens.USER_READ_QR}
        component={ReadQrScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header: () => <Header title={'Read QR'} rightComponent={<CloseButton onPress={handleBackNavigation} />} />
        }} />
    </Stack.Navigator>
  );
};
