import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { screens } from '../consts/screens';
import { Header } from '../../base/components/Header';
import { MainScreen } from '../../screens/tabs/MainScreen';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screens.ADMINISTRATOR_STACK}
        component={MainScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header:()=><Header title={'Администратор'}/>
        }} />
    </Stack.Navigator>
  );
};
