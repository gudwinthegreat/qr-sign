import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { screens } from '../consts/screens';
import { Text } from 'react-native';
import { Header } from '../../base/components/Header';
import { CollectionHomeScreen } from '../../screens/tabs/collection/CollectionHomeScreen';

const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screens.COLLECTION_STACK}
        component={CollectionHomeScreen}
        options={{
          headerShown: true,
          headerMode: 'screen',
          header:()=><Header title={'Коллекция'}/>
        }} />
    </Stack.Navigator>
  );
};
