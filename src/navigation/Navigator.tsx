import React from 'react';
import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { observer } from 'mobx-react';

import { screens } from './consts/screens';
import Navigation from '../base/Navigation';

import { Colors } from '../styles/Colors';
import { TabsStack } from './stacks/TabsStack';

const Stack = createStackNavigator();

const Navigator = observer(() => {
  const AppTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: Colors.white,
    },
  };

  return (
    <NavigationContainer theme={AppTheme} ref={Navigation.navigationRef}>
      <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={screens.INIT}>
        {TabsStack}
      </Stack.Navigator>
    </NavigationContainer>
  );
});

export default Navigator;
