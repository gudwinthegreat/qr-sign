import AsyncStorage from '@react-native-async-storage/async-storage';

export default abstract class AbstractLocalRepository {
  abstract tableName(): string;

  get = async (key?: string): Promise<any> => {
    const data = await AsyncStorage.getItem(key || this.tableName());
    return data ? JSON.parse(data) : null;
  };

  set = async (data: any, key?: string) => {
    return AsyncStorage.setItem(key || this.tableName(), JSON.stringify(data));
  };

  update = async (data: any) => {
    let res = await this.get();

    if (res) {
      for (let k in data) {
        res[k] = data[k];
      }
    }

    return this.set(res).then();
  };

  removeAll = async () => {
    return AsyncStorage.removeItem(this.tableName());
  };

  // remove
}
