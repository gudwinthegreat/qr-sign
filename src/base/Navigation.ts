import * as React from 'react';
import { CommonActions, NavigationContainerRef, StackActions } from '@react-navigation/core';

import { screens } from '../navigation/consts/screens';

export interface NavigationParams {
  [key: string]: any;
}

class NavigationC {
  navigationRef = React.createRef<NavigationContainerRef<any>>();

  initialRoute = screens.tab.TAB_USER;

  setInitialRoute = (route: string) => {
    this.initialRoute = route;
  };

  navigate = (routeName: string, params?: NavigationParams) => {
    // https://github.com/react-navigation/react-navigation/issues/6879
    setTimeout(() => this.navigationRef.current?.navigate(routeName, params), 0);
  };

  replace = (routeName: string, params?: NavigationParams) => {
    // https://github.com/react-navigation/react-navigation/issues/6879
    setTimeout(
      () =>
        this.navigationRef.current?.reset({
          index: 0,
          routes: [{ name: routeName, params: params }],
        }),
      0,
    );
  };

  pop = () => {
    this.navigationRef.current?.goBack();
  };

  pop2 = () => {
    this.pop();
    this.pop();
  };

  /**
   * Перезаписывает последние две страницы на выбранную.
   * @param {string} routeName - Название страницы.
   */
  replaceTwoScreens = (routeName: string) => {
    this.navigationRef.current?.dispatch((state:any) => {
      const routes = state.routes.slice(0, -2);

      return CommonActions.reset({
        ...state,
        index: routes.length,
        routes: [...routes, { name: routeName, params: undefined }],
      });
    });
  };

  /**
   * Сбрасывает всю историю навигации до главной страницы.
   */
  resetToHome = () => {
    this.navigationRef.current?.resetRoot({
      index: 1,
      routes: [{ name: screens.MAIN_APP }],
    });
  };

  /**
   * Сбрасывает всю историю навигации до главной страницы и переданной сраницы.
   * @param {string} routeName - Название страницы.
   * @param {NavigationParams} params - Параметры страницы.
   */
  resetToScreen = (routeName: string, params?: NavigationParams) => {
    this.navigationRef.current?.resetRoot({
      index: 1,
      routes: [{ name: screens.MAIN_APP }, { name: routeName, params: params }],
    });
  };

  /**
   * Возвращение к самому первому экрану стэка, и если возможно сделать навигацию на одну страницу назад.
   */
  popToTop = () => {
    this.navigationRef.current?.dispatch(StackActions.popToTop());
  };
}

const Navigation = new NavigationC();
export default Navigation;
