import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Colors } from '../../styles/Colors';

import { CloseIcon } from './icons/CloseIcon';



interface ICloseButton {
  onPress: () => void;
}

export const CloseButton = ({ onPress }: ICloseButton) => {
  return (
    <TouchableOpacity style={styles.containerButton} onPress={onPress}>
      <CloseIcon />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerButton: {
    height: 32,
    width: 32,
    borderRadius: 32,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.fillsNeutral200,
  },
});
