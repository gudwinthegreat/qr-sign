import React, { ReactNode } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { AgEnum, Text } from './Text';

import Navigation from '../Navigation';

import { Colors } from '../../styles/Colors';
import { BackIcon } from './icons/BackIcon';

interface IHeaderProps {
  title?: string;
  showBack?: boolean;
  rightComponent?: ReactNode;
}

export const Header = ({ showBack, title, rightComponent }: IHeaderProps) => {
  const insets = useSafeAreaInsets();

  const handleBackNavigation = () => {
    Navigation.pop();
  };

  const renderTitle = () => {
    if (!showBack && title?.length !== 0) {
      return (
        <View style={styles.headerCenter}>
          <Text style={styles.textTitle} Ag={AgEnum.H400}>{title}</Text>
        </View>
      );
    }

    if (title?.length !== 0) {
      return (
        <View style={[styles.headerCenter, styles.pl8]}>
          <Text Ag={AgEnum.BW400} color={Colors.textSecondary2} style={styles.textTitle}>
            {title}
          </Text>
        </View>
      );
    }

    return null;
  };

  return (
    <View style={[styles.header, { marginTop: 12 + insets.top }]}>
      {showBack ? (
        <TouchableOpacity onPress={handleBackNavigation} style={styles.row}>
          <View style={styles.headerSide}>
            <View>
              <BackIcon />
            </View>
          </View>
          {renderTitle()}
        </TouchableOpacity>
      ) : (
        renderTitle()
      )}

      <View style={styles.containerRightComponent}>{rightComponent}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 42,
    backgroundColor: Colors.white,
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerCenter: {
    flex: 1,
    marginRight: 16,
  },
  headerSide: {
    width: 24,
  },
  pl8: {
    paddingLeft: 8,
  },
  row: {
    flexDirection: 'row',
    flex: 1,
  },
  containerRightComponent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    // flexShrink: 1,
    flexWrap: 'nowrap',
    
    // ellipsizeMode:"tail"
  },
});
