import React, { useState } from 'react';
import { StyleProp, StyleSheet, TextInput, TextInputProps, View, ViewStyle } from 'react-native';
import TextInputMask from 'react-native-text-input-mask';

import { AgEnum, Text } from './Text';

import { Colors } from '../../styles/Colors';

interface IInputProps extends TextInputProps {
    value?: string;
    inputKey: string;
    headerPlaceholder: string;
    mask?: string;
    containerStyle?: StyleProp<ViewStyle>;
    leftIcon?: JSX.Element;
    handleChange: (value: string) => void;
    hint?: string;
    getRef?: React.MutableRefObject<null>
}

export const IInput = (props: IInputProps) => {
    const [isFocused, setFocus] = useState<boolean>(false);

    return (
        <>
            <View style={[styles.container, props.containerStyle]}>
                {props.headerPlaceholder ? (
                    <View style={styles.label}>
                        <View>
                            <Text Ag={AgEnum.RW400} color={Colors.textSecondary}>
                                {props.headerPlaceholder}
                            </Text>
                        </View>
                    </View>
                ) : null}
                <View style={[styles.content, isFocused && styles.focusContent]}>
                    {props.inputKey === 'mask' ? (
                        <View style={styles.containerMaskedIcon}>
                            {props.leftIcon && <View style={styles.containerIcon}>{props.leftIcon}</View>}
                            <TextInputMask
                                {...props}
                                keyboardType="number-pad"
                                returnKeyType="done"
                                mask={props?.mask?.toString() || ''}
                                onChange={e => props.handleChange(e.nativeEvent.text)}
                                placeholderTextColor={Colors.trietary}
                                value={props.value}
                                onFocus={() => setFocus(true)}
                                onBlur={() => setFocus(false)}
                                style={styles.input}
                                ref={props.getRef}
                            />
                        </View>
                    ) : (
                        <TextInput
                            {...props}
                            autoCapitalize="none"
                            value={props.value}
                            onChangeText={props.handleChange}
                            placeholderTextColor={Colors.trietary}
                            onFocus={() => setFocus(true)}
                            onBlur={() => setFocus(false)}
                            style={styles.input}
                        />
                    )}
                </View>
                {props.hint ? (<Text Ag={AgEnum.RW400} color={Colors.textSecondary2}>
                    Комментарий: {props.hint}
                </Text>) : null}
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        position: 'relative',
        backgroundColor: Colors.white,
    },
    label: {
        zIndex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginBottom: 6,
    },
    // height was 56
    content: {
        flexDirection: 'row',
        alignItems: 'stretch',
        overflow: 'hidden',
        borderRadius: 4,
        flexShrink:1
    },
    focusContent: {
        borderWidth: 2,
        borderColor: Colors.brand300,
    },
    input: {
        flex: 1,
        backgroundColor: Colors.fillsNeutral300,
        color: Colors.textPrimary,
        paddingHorizontal: 14,
        paddingTop:4,
        paddingBottom:8,
        fontSize: 32,
        lineHeight: 40,
        fontWeight: '500',
        fontFamily: 'Inter',        
    },
    containerIcon: {
        marginLeft: 14,
    },
    containerMaskedIcon: {
        backgroundColor: Colors.fillsNeutral300,
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    measureUnit: {
        marginRight: 10
    },
});
