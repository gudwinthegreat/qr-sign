import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const MoreIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M5 10.2695C3.9 10.2695 3 11.1695 3 12.2695C3 13.3695 3.9 14.2695 5 14.2695C6.1 14.2695 7 13.3695 7 12.2695C7 11.1695 6.1 10.2695 5 10.2695ZM19 10.2695C17.9 10.2695 17 11.1695 17 12.2695C17 13.3695 17.9 14.2695 19 14.2695C20.1 14.2695 21 13.3695 21 12.2695C21 11.1695 20.1 10.2695 19 10.2695ZM12 10.2695C10.9 10.2695 10 11.1695 10 12.2695C10 13.3695 10.9 14.2695 12 14.2695C13.1 14.2695 14 13.3695 14 12.2695C14 11.1695 13.1 10.2695 12 10.2695Z"
        fill={color || Colors.textSecondary2}
      />
    </Svg>
  );
};
