import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const ChevronUpIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M12 10.828L7.05001 15.778L5.63601 14.364L12 8L18.364 14.364L16.95 15.778L12 10.828Z"
        fill={color || Colors.textPrimary}
      />
    </Svg>
  );
};
