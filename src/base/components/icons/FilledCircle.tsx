import React from 'react';
import Svg, { Circle } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';

export const FilledCircle = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
        <Circle
        cx={10} 
        cy={10}
        r={10}
        fill={color || "white"}
        />
    </Svg>
  );
};