import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const DownArrow = ({ size, color }: IIconProps) => {
    return (
        <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
            <Path
                d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"
                fill={color || Colors.green}
            />
        </Svg>
    );
};
