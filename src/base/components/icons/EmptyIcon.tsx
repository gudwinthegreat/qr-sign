import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const EmptyIcon = ({ size, color }: IIconProps) => {
    return (
        <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M12 3.75a8.25 8.25 0 100 16.5 8.25 8.25 0 000-16.5zM2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12z"
                fill={color || Colors.textSecondary2}
            />
        </Svg>
    );
};
