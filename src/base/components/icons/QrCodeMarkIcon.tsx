import React from 'react';
import Svg, { Path } from 'react-native-svg';

export const QrCodeMarkIcon = () => {
  return (
    <Svg width="244" height="244" viewBox="0 0 244 244">
      <Path d="M53 1.5H1.5V53.5" stroke="white" stroke-width="2" />
      <Path d="M1.25 189.25L1.25 240.75H53.25" stroke="white" stroke-width="2" />
      <Path d="M191 241H242.5V189" stroke="white" stroke-width="2" />
      <Path d="M242.75 52.7495V1.24951L190.75 1.24951" stroke="white" stroke-width="2" />
    </Svg>
  );
};
