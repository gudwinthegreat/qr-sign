import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const NotificationIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M22 20.7305H2V18.7305H3V11.7615C3 6.77347 7.03 2.73047 12 2.73047C16.97 2.73047 21 6.77347 21 11.7615V18.7305H22V20.7305ZM5 18.7305H19V11.7615C19 7.87847 15.866 4.73047 12 4.73047C8.134 4.73047 5 7.87847 5 11.7615V18.7305ZM9.5 21.7305H14.5C14.5 22.3935 14.2366 23.0294 13.7678 23.4982C13.2989 23.9671 12.663 24.2305 12 24.2305C11.337 24.2305 10.7011 23.9671 10.2322 23.4982C9.76339 23.0294 9.5 22.3935 9.5 21.7305Z"
        fill={color || Colors.trietary}
      />
    </Svg>
  );
};
