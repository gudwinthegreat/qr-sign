import React from 'react';
import Svg, { G, Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const InfoCircleIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <G stroke={color || Colors.textSecondary2} strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round">
        <Path d="M12 21a9 9 0 100-18 9 9 0 000 18zM12 8h.01" />
        <Path d="M11 12h1v4h1" />
      </G>
    </Svg>
  );
};
