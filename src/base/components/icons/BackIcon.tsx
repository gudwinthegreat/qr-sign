import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const BackIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M7.828 11.0002H20V13.0002H7.828L13.192 18.3642L11.778 19.7782L4 12.0002L11.778 4.22217L13.192 5.63617L7.828 11.0002Z"
        fill={color || Colors.textSecondary2}
      />
    </Svg>
  );
};
