import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const TimeIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M12 22.7305C6.477 22.7305 2 18.2535 2 12.7305C2 7.20747 6.477 2.73047 12 2.73047C17.523 2.73047 22 7.20747 22 12.7305C22 18.2535 17.523 22.7305 12 22.7305ZM12 20.7305C14.1217 20.7305 16.1566 19.8876 17.6569 18.3873C19.1571 16.887 20 14.8522 20 12.7305C20 10.6087 19.1571 8.57391 17.6569 7.07361C16.1566 5.57332 14.1217 4.73047 12 4.73047C9.87827 4.73047 7.84344 5.57332 6.34315 7.07361C4.84285 8.57391 4 10.6087 4 12.7305C4 14.8522 4.84285 16.887 6.34315 18.3873C7.84344 19.8876 9.87827 20.7305 12 20.7305ZM13 12.7305H17V14.7305H11V7.73047H13V12.7305Z"
        fill={color || Colors.trietary}
      />
    </Svg>
  );
};
