import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { IIconProps } from '../../../screens/types/Icon';
import { Colors } from '../../../styles/Colors';

export const ChevronRightIcon = ({ size, color }: IIconProps) => {
  return (
    <Svg width={size || '24'} height={size || '24'} viewBox="0 0 24 24">
      <Path
        d="M13.1719 12.7305L8.22192 7.78046L9.63592 6.36646L15.9999 12.7305L9.63592 19.0945L8.22192 17.6805L13.1719 12.7305Z"
        fill={color || Colors.textPrimary}
      />
    </Svg>
  );
};
