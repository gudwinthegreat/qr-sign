import React, { ReactNode, useMemo } from 'react';
import {
  ActivityIndicator,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';

import { AgEnum, Text } from './Text';

import { Colors } from '../../styles/Colors';

export enum ButtonType {
  Filled = 'filled',
  Danger = 'danger',
  FilledInvert = 'filledInvert',
  Outline = 'outline',
  OutlineInvert = 'outlineInvert',
  Transparent = 'transparent',
}

interface IButtonProps extends TouchableOpacityProps {
  title: string;
  startIcon?: ReactNode;
  endIcon?: ReactNode;
  type?: ButtonType;
  style?: StyleProp<ViewStyle>;
  loading?: boolean;
  size?: 'large' | 'medium' | 'small';
  textColor?: string;
  disabled?: boolean;
}

export const Button = (props: IButtonProps) => {
  const {
    title,
    startIcon,
    endIcon,
    type = ButtonType.Filled,
    style,
    loading,
    size = 'large',
    textColor,
    disabled,
  } = props;

  const color = useMemo(() => {
    if (disabled) {
      switch (type) {
        case ButtonType.Outline:
        case ButtonType.Transparent:
          return Colors.gray;
        case ButtonType.OutlineInvert:
          return Colors.greenPaleBack;
      }
    }

    switch (type) {
      case ButtonType.FilledInvert:
      case ButtonType.Outline:
      case ButtonType.Transparent:
        return textColor || Colors.brand300;

      default:
        return textColor || Colors.white;
    }
  }, [type, textColor, disabled]);

  const buttonStyles = useMemo(() => {
    return [styles.default, disabled ? styles[`${type}_Disabled`] : styles[type], styles[size], style];
  }, [style, type, size, disabled]);

  return (
    <TouchableOpacity
      onPress={loading || disabled ? undefined : props.onPress}
      style={buttonStyles}
      activeOpacity={props.activeOpacity}
    >
      {!loading && startIcon}
      {!loading ? (
        <Text Ag={AgEnum.Button} color={color} style={{ marginLeft: startIcon ? 8 : 0, marginRight: endIcon ? 8 : 0 }}>
          {title}
        </Text>
      ) : (
        <ActivityIndicator color={color} />
      )}
      {!loading && endIcon}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  default: {
    height: 56,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  [ButtonType.Filled]: {
    backgroundColor: Colors.brand300,
  },
  [`${ButtonType.Filled}_Disabled`]: {
    backgroundColor: Colors.brand04,
  },
  [`${ButtonType.Filled}_Text`]: {
    color: Colors.white,
  },

  [ButtonType.Danger]: {
    backgroundColor: Colors.red,
  },
  [`${ButtonType.Danger}_Disabled`]: {
    backgroundColor: Colors.red01,
  },
  [`${ButtonType.Danger}_Text`]: {
    color: Colors.white,
  },

  [ButtonType.FilledInvert]: {
    backgroundColor: Colors.brandPale,
  },
  [`${ButtonType.FilledInvert}_Disabled`]: {
    backgroundColor: Colors.brandPale,
  },
  [`${ButtonType.FilledInvert}_Text`]: {
    color: Colors.brand300,
  },

  [ButtonType.Outline]: {
    borderWidth: 1,
    borderColor: Colors.brand300,
  },
  [`${ButtonType.Outline}_Disabled`]: {
    borderWidth: 1,
    borderColor: Colors.white,
  },
  [`${ButtonType.Outline}_Text`]: {
    color: Colors.brand300,
  },

  [ButtonType.OutlineInvert]: {
    borderWidth: 1,
    borderColor: Colors.white,
  },
  [`${ButtonType.OutlineInvert}_Disabled`]: {
    borderWidth: 1,
    borderColor: Colors.brand300,
  },
  [`${ButtonType.OutlineInvert}_Text`]: {
    color: Colors.brand300,
  },

  large: {
    height: 48,
  },
  medium: {
    height: 40,
  },
  small: {
    height: 32,
  },
});
