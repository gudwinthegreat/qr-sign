import React from 'react';
import { StyleSheet, Text as RNText, TextProps } from 'react-native';

import { Colors } from '../../styles/Colors';

export enum AgEnum {
  RW300 = 'RW300',
  RW400 = 'RW400',
  RW500 = 'RW500',
  RW600 = 'RW600',
  H100 = 'H100',
  H200 = 'H200',
  H300 = 'H300',
  H400 = 'H400',
  H500 = 'H500',
  H900 = 'H900',
  BW300 = 'BW300',
  BW400 = 'BW400',
  BW500 = 'BW500',
  RN300 = 'RN300',
  RN400 = 'RN400',
  RN500 = 'RN500',
  Button = 'Button',
  Avatar = 'Avatar',
  Badge = 'Badge',
  BottomBadge = 'BottomBadge',
}

interface IText extends TextProps {
  Ag: AgEnum;
  children?: string | React.ReactNode[];
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  color?: string;
}

export const Text = (props: IText) => {
  const { Ag, color, align } = props;

  return (
    <RNText
      {...props}
      style={[
        styles[Ag],
        {
          fontFamily: 'Inter',
          color: color || Colors.textPrimary,
          textAlign: align || 'auto',
        },
        props.style,
      ]}
    />
  );
};

const styles = StyleSheet.create({
  [AgEnum.RW300]: {
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '400',
  },
  [AgEnum.RW400]: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '400',
  },
  [AgEnum.RW500]: {
    fontSize: 18,
    lineHeight: 28,
    fontWeight: '400',
  },
  [AgEnum.RW600]: {
    fontSize: 20,
    lineHeight: 32,
    fontWeight: '400',
  },
  [AgEnum.H100]: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '700',
  },
  [AgEnum.H200]: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: '700',
  },
  [AgEnum.H300]: {
    fontSize: 20,
    lineHeight: 26,
    fontWeight: '700',
  },
  [AgEnum.H400]: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: '700',
  },
  [AgEnum.H500]: {
    fontSize: 28,
    lineHeight: 36,
    fontWeight: '700',
  },
  [AgEnum.H900]: {
    fontSize: 48,
    lineHeight: 56,
    fontWeight: '700',
  },
  [AgEnum.BW300]: {
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '500',
  },
  [AgEnum.BW400]: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '500',
  },
  [AgEnum.BW500]: {
    fontSize: 18,
    lineHeight: 28,
    fontWeight: '500',
  },
  [AgEnum.RN300]: {
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '400',
  },
  [AgEnum.RN400]: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '400',
  },
  [AgEnum.RN500]: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: '400',
  },
  [AgEnum.Button]: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: '500',
  },
  [AgEnum.Avatar]: {
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '500',
  },
  [AgEnum.Badge]: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '500',
  },
  [AgEnum.BottomBadge]: {
    fontSize: 12,
    lineHeight: 18,
    fontWeight: '700',
  },
});
