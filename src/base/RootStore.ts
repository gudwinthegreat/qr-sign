import React from "react";
import { MainStore } from "../modules/main/MainStore";
import { UserStore } from "../modules/user/UserStore";

class RootStore {
  userStore: UserStore;
  mainStore: MainStore;

  constructor() {
    this.userStore = new UserStore();
    this.mainStore = new MainStore();
  }

  /**
   * Фнукция проходит по всем сторисам и вызывает функцию sync если она есть.
   * Функция вызывается в файле App.tsx
   */
  sync = async () => {
    await Promise.all(
      Object.values(this).map((store) => {
        return store?.sync ? store?.sync() : Promise.resolve();
      })
    );
  };
}

const rootStore = new RootStore();

export type RootStoreContext = React.Context<RootStore> & {
  _currentValue: RootStore;
};

export const storesContext = React.createContext(rootStore);
