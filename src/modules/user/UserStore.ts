import { makeAutoObservable } from "mobx";

export class UserStore {
  checkString: string;
  isCheckStringEmpty: boolean = true;
  constructor() {
    makeAutoObservable(this);
    this.checkString = "";
  }

  setCheckString = (value: string) => {
    this.checkString = value;
    if (value) {
      this.isCheckStringEmpty = false;
    } else {
      this.isCheckStringEmpty = true;
    }
  };

  sync = () => {
    console.log("userStore sync");
  };
}
