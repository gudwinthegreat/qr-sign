import { makeAutoObservable } from "mobx";
import { Role } from "./models/MainModels";
import { screens } from "../../navigation/consts/screens";

export class MainStore {
  role: Role;
  constructor() {
    makeAutoObservable(this);
    this.role = Role.USER;
  }

  public setRole = (value: string | typeof screens) => {
    if (value === screens.tab.TAB_ADMINISTRATOR) {
      this.role = Role.ADMINISTRATOR;
    }
    if (value === screens.tab.TAB_USER) {
      this.role = Role.USER;
    }
    if (value === screens.tab.TAB_COLLECTION) {
      this.role = Role.COLLECTION;
    }
  };
}
