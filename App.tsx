import React, { useEffect } from 'react';
// import { StyleSheet } from 'react-native';

import { StyleSheet, View, Text } from 'react-native';
import { observer } from 'mobx-react-lite';
import { enableScreens } from 'react-native-screens';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import moment from 'moment';

import 'moment/locale/ru';
// import './src/base/adapters/KeyboardManagerAdapter';

import { useRootStore } from './src/base/hooks/useRootStore';

import Navigator from './src/navigation/Navigator';

import { Colors } from './src/styles/Colors';

enableScreens();
moment.locale('ru');

// if (Platform.OS === 'android') {
//   StatusBar.setTranslucent(true);
//   StatusBar.setBackgroundColor('transparent');
// }

// StatusBar.setBarStyle('dark-content');

const App = observer(() => {
  const { sync } = useRootStore();

  useEffect(() => {
    (async () => {
      await sync();

    })();
  }, []);

  return (
    <SafeAreaProvider style={styles.container}>
      <Navigator />
      {/* <View style={{}} >
        <Text >
          SOME TEXT
        </Text>
      </View> */}
    </SafeAreaProvider>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});

export default App;